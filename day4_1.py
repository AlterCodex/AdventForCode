def isValid(data):
    d = {}
    for t in data:
        if t in d:
            return False
        else:
            d[t] = 0
    return True

''.join(sorted(a))
raw = "1"
res = 0

while raw != '':
    raw = input()

    if raw != '' and isValid(raw.split(' ')):
        res = res + 1

print(res)
