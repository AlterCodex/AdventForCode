"""
Outputs the closest square spiral matrix of an input number
"""
from math import sqrt,ceil,fabs


def isAbajo(data, limit, root):
    return limit - root  +1 <= data <= limit


def isIzquierda(data, limit, root):
    return limit - 2 * root + 2 <= data < limit - root + 1


def isArriba(data, limit, root):
    return limit - 3 * root + 3 <= data < limit - 2 * root + 2


def isDerecha(data, limit, root):
    return (root - 2) ** 2 < data < limit - 3 * root + 3


data = int(input("number:"))
root = ceil(sqrt(data))
if root % 2 == 0:
    root = root + 1
limit = root * root
print (root)
print (limit)
mitad=1
distancia=int(root/2)
if isAbajo(data, limit, root):
    mitad = (limit - root + limit + 1)/2
    print(mitad)
if isIzquierda(data, limit, root):
    mitad =(limit - 2*root + 1 + limit - root + 2)/2
    print(mitad)
if isArriba(data, limit, root):
    mitad =(limit - 3 * root + 3 + limit - 2*root + 2) /2
    print(mitad)
if isDerecha(data, limit, root):
    mitad = ((root - 2) ** 2 + limit - 3 * root + 3 )/2
    print(mitad)
distancia =distancia + fabs(data-mitad)
print(distancia)