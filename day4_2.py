def isValid(data):
    d = {}
    for a in data:
        t=''.join(sorted(a))
        if t in d:
            return False
        else:
            d[t] = 0
    return True


raw = "1"
res = 0

while raw != '':
    raw = input()

    if raw != '' and isValid(raw.split(' ')):
        res = res + 1

print(res)
