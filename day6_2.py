

def findMax(data):
    max_value = -1
    max_index = 0
    for i in range(0, len(data)):
        if data[i] > max_value:
            max_index = i
            max_value = data[i]
    return max_index


data = list()
val = input()
val = val.replace('\t', ' ')
strings = val.split(' ')
for s in strings:
    if s != " ":
        data.append(int(s))
i =0
sec = {"": 0}
sec.clear()
key = ','.join(map(str, data))
pasos = 0
while key not in sec:
    sec[key] = 0
#    print(sec)
    max_pos = findMax(data)
    #print(max_pos)
    value = data[max_pos]
    data[max_pos] = 0
    while value > 0:
        max_pos = max_pos+1
        if max_pos >= len(data):
            max_pos = 0
        value = value - 1
        data[max_pos] = data[max_pos] + 1
    #print(data)
    key = ','.join(map(str, data))
for s in sec:
    pasos += 1
    if s == key:
        break
pasos = len(sec)-pasos +1
print(pasos)